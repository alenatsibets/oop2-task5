public class Airline {
    private final String destination;
    private final int flightNumber;
    private final String aircraftType;
    private final String departureTime;
    private final String daysOfTheWeek;

    public Airline(String destination, int flightNumber, String aircraftType, String departureTime, String daysOfTheWeek) {
        this.destination = destination;
        this.flightNumber = flightNumber;
        this.aircraftType = aircraftType;
        this.departureTime = departureTime;
        this.daysOfTheWeek = daysOfTheWeek;
    }

    public String getDestination() {
        return destination;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public String getDaysOfTheWeek() {
        return daysOfTheWeek;
    }

    @Override
    public String toString() {
        return "Airline{" +
                "destination='" + destination + '\'' +
                ", flightNumber=" + flightNumber +
                ", aircraftType='" + aircraftType + '\'' +
                ", departureTime='" + departureTime + '\'' +
                ", daysOfTheWeek='" + daysOfTheWeek + '\'' +
                '}';
    }
}
