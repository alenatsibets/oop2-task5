import java.util.Comparator;

public class AirlineComparator implements Comparator<Airline> {
    @Override
    public int compare(Airline o1, Airline o2) {
        String time1 = o1.getDepartureTime();
        String time2 = o2.getDepartureTime();
        return time1.compareTo(time2);
    }
}
