import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class DataBase {
    private final Airline[] airlines;
    private int length;
    public DataBase() {
        this.airlines = new Airline[5];
        this.length = 0;
    }
    public void add(Airline a){
        airlines[length] = a;
        length++;
    }

    public void listForDestination(){
        Scanner sc = new Scanner(System.in);
        String destination = sc.nextLine();
        for (int i = 0; i < 5; i++){
            if (airlines[i].getDestination().equals(destination)){
                System.out.println(airlines[i]);
            }
        }
    }

    public List<Airline> listDayOfWeekWith(){
        Scanner sc = new Scanner(System.in);
        String day = sc.nextLine();
        List<Airline> byDay = new LinkedList<>();
        for (int i = 0; i < 5; i++){
            if (airlines[i].getDaysOfTheWeek().equals(day)){
                byDay.add(airlines[i]);
            }
        }
        return byDay;
    }
}
