import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        DataBase base = new DataBase();
        base.add(new Airline("Paris", 209, "Ryanair", "18:20", "Mon"));
        base.add(new Airline("Paris", 168, "AirBaltic", "15:20", "Sun"));
        base.add(new Airline("London", 642, "Ryanair", "21:55", "Tue"));
        base.add(new Airline("Cardiff", 733, "KLM", "07:50", "Sun"));
        base.add(new Airline("Tbilisi", 444, "KLM", "19:10", "Mon"));

        System.out.println("Enter 1 to see a list of flights for a given destination.");
        System.out.println("Enter 2 to see a list of flights for a given day of the week.");
        System.out.println("Enter 3 to see a list of flights for a given day of the week, the departure time for which is longer than the specified one.");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        if (number == 1){
            base.listForDestination();
        } else if (number == 2) {
            List<Airline> byDay = base.listDayOfWeekWith();
            for (Airline element: byDay) {
                System.out.println(element);
            }
        } else if (number == 3) {
            List<Airline> byDay = base.listDayOfWeekWith();
            byDay.sort(new AirlineComparator());
            for (Airline element: byDay) {
                System.out.println(element);
            }
        } else {
            throw new RuntimeException("Incorrect value");
        }
    }
}